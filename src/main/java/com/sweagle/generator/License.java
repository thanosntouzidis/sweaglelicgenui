package com.sweagle.generator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represent the license information. The {@link License} class should be used to store related information
 * about the license. This information may then be saved into an encrypted file.
 * 
 * @author 
 */
public final class License {

    public static final String COMPANY = "Company";
    public static final String EMAIL = "Email";
    public static final String EXPIRATION = "Expiration_Date";
//    public static final String ID = "Id";

//    public static final String LICENSE_NUMBER = "licenseNumber";
    public static final String LICENSE_TYPE = "License_Type";
    public static final String TYPE_LIFETIME = "Lifetime";
    public static final String TYPE_SINGLE_VERSION = "Single-version";
    public static final String TYPE_TRIAL = "Trial";
    public static final String VERSION = "Version";

    /**
     * Map to store properties.
     */
    private final Map<String, String> properties;
    
    public License(String companyName, String expDate, String licenseType, String version) {
        this.properties = new HashMap<>();
        properties.put(LICENSE_TYPE, licenseType);
        properties.put(COMPANY, companyName);
        properties.put(EXPIRATION, expDate);
        properties.put(VERSION, version);
    }

    /**
     * Return an unmodifiable map of properties.
     * 
     * @return
     */
    public Map<String, String> getProperties() {
        return Collections.unmodifiableMap(this.properties);
    }

    /**
     * Get the property value.
     * 
     * @param key
     *            the property key
     * @return 
     */
    public String getProperty(String key) {
        return this.properties.get(key);
    }
}
