package com.sweagle.generator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * This class is used to manage the encryption of the license. It's used to encrypt, sign and validate using a public or
 * private key.
 */
public class EncryptionManager {

    private static final int BUF_SIZE = 4096;
    private PrivateKey privateKey;

    /**
     * Create a new encryption manager.
     * @param privateKey
     *            the private key (null if not available).
     * @throws NoSuchAlgorithmException
     *             if no Provider supports RSA
     * @throws InvalidKeySpecException
     *             if the given key specification is inappropriate for this key factory to produce a public key.
     */
    public EncryptionManager(byte[] privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        if (privateKey == null) {
            throw new NullPointerException("privateKey");
        }
        PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(privateKey);
        KeyFactory privateKeyFactory = KeyFactory.getInstance("RSA");
        this.privateKey = privateKeyFactory.generatePrivate(privateSpec);
    }

    /**
     * This function maybe used to read the public and/or private key from a file.
     * 
     * @param file
     *            the file to read
     * @return the file data
     * 
     * @throws IOException
     *             if the file does not exist, or if the first byte cannot be read for any reason
     */
    public static byte[] readAll(File file) throws IOException {
        try (InputStream input = new FileInputStream(file)) {
            // Read the content of the file and store it in a byte array.
            ByteArrayOutputStream out = new ByteArrayOutputStream(BUF_SIZE);
            byte[] buf = new byte[BUF_SIZE];
            int size;
            while ((size = input.read(buf)) != -1) {
                out.write(buf, 0, size);
            }
            return out.toByteArray();
        }
    }

    /**
     * Sign the given input stream data. The signature is append to the output stream.
     * 
     * @param data
     *            the the data to be signed.
     * @return the signature for the given data.
     * @throws NoSuchAlgorithmException
     *             if no Provider supports a Signature implementation for SHA1withRSA.
     * @throws InvalidKeyException
     *             if the private key is invalid.
     * @throws SignatureException
     *             if this signature algorithm is unable to process the input data provided.
     * @throws UnsupportedOperationException
     *             if the private key was not provided in the constructor.
     */
    public byte[] sign(byte[] data) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        if (privateKey == null) {
            throw new UnsupportedOperationException("Can't sign when the private key is not available.");
        }
        // Initialize the signing algorithm with our private key
        Signature rsaSignature = Signature.getInstance("SHA1withRSA");
        rsaSignature.initSign(privateKey);
        rsaSignature.update(data);
        // Generate the signature.
        return rsaSignature.sign();
    }
}
