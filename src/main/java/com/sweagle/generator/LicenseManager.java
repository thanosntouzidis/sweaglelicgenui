package com.sweagle.generator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * This the main entry point of the licensing module. This class should be used
 * to create and check license files.
 * <p>
 * Generally, an application will not required more then one instance of license
 * manager.
 */
public class LicenseManager {
    
    private static final String SIGNATURE = "Signature";
    private final EncryptionManager encryptionManager;
    

    /**
     * Create a new license manager.
     * 
     * @param privateKey
     *            the private key file (null if not available).
     * @throws GeneralSecurityException
     *             if the provided key are invalid.
     * @throws IOException
     *             if the file doesn't exists
     */
    public LicenseManager(File privateKey) throws GeneralSecurityException, IOException {
        byte[] privdata = null;
        if (privateKey != null) {
            privdata = EncryptionManager.readAll(privateKey);
        }
        this.encryptionManager = new EncryptionManager(privdata);
    }

    /**
     * Used to serialize a license object.
     * 
     * @param lic
     * @param file
     *            the location where to save the new license file. If file
     *            exists, it's overwrite.
     * @throws IOException
     *             if the file doesn't exists or can't be written to
     * @throws SignatureException
     *             if this signature algorithm is unable to process the license
     *             data
     * @throws NoSuchAlgorithmException
     *             if the algorithm SHA is not supported
     * @throws InvalidKeyException
     *             if the private key is invalid.
     */
    public void writeLicense(License lic, File file) throws IOException, InvalidKeyException, NoSuchAlgorithmException, SignatureException {
        byte[] data = writeLicenseToByteArray(lic);
        // Then sign the byte array
        byte[] signature = this.encryptionManager.sign(data);
        String base64signature = Base64.encode(signature);
        // Create property file
        Properties prop = new Properties();
        for (Entry<String, String> e : lic.getProperties().entrySet()) {
            prop.setProperty(e.getKey(), e.getValue());
        }
        prop.put(SIGNATURE, base64signature);
        // Write the property file
        prop.store(new FileWriter(file), "License file");
        try {
            encrypt();
        } catch (Exception ex) {
            Logger.getLogger(LicenseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String encrypt() throws Exception{
        String strData="";

        String key = "mmmmmmmm";
        String clearText = readFile("C:\\licenses\\license.txt");
        try {
            SecretKeySpec skeyspec=new SecretKeySpec(key.getBytes(),"Blowfish");
            Cipher cipher=Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
            byte[] encrypted=cipher.doFinal(clearText.getBytes());
            strData=Base64.encode(encrypted);
            
            FileWriter fw = new FileWriter("C:\\licenses\\licenseBlowfish.txt");
            File myLicFileBlowFish = new File("C:\\licenses\\licenseBlowfish.txt");
            writeTextToFile(myLicFileBlowFish, strData);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }
    
    public static void writeTextToFile(File myLicenseFile, String encrypted) throws IOException{
        
        try {
            Files.write(Paths.get("C:\\licenses\\licenseBlowfish.txt"), encrypted.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            System.out.println("lathos");
        }
    }
    
    static String readFile(String path) throws IOException{
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded);
    }

    /**
     * Write the license information into a byte array ready to be signed.
     * 
     * @param lic
     *            the license
     * @return the byte array
     * @throws IOException
     */
    protected byte[] writeLicenseToByteArray(License lic) throws IOException {
        ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(dataStream);
        // Sort the key to have a predictable results.
        List<String> keys = new ArrayList<>(lic.getProperties().keySet());
        Collections.sort(keys);
        for (String key : keys) {
            String value = lic.getProperty(key);
            out.writeChars(key);
            out.writeChars(value);
        }
        out.flush();
        byte[] data = dataStream.toByteArray();
        out.close();
        return data;
    }
}
