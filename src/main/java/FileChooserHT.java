import javax.swing.JButton;
import javax.swing.JFileChooser;

public class FileChooserHT {
    
    public static String chooseFile(){
        JButton open = new JButton();
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new java.io.File("C:/"));
        fc.setDialogTitle("Choose a file");
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        if(fc.showOpenDialog(open) == JFileChooser.APPROVE_OPTION){
        }
        return fc.getSelectedFile().getAbsolutePath();
    }
}
