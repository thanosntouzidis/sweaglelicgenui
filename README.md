Private and Public keys generation 
	Create the private key (containing information to create the public key).
		$ openssl genrsa -out privkey.pem 2048
		$ openssl pkcs8 -topk8 -in privkey.pem -inform PEM -nocrypt -outform DER -out privkey.
	Extract the public key, for publishing.
		$ openssl rsa -in privkey.pem -out pubkey.der -pubout -outform DER



Details

	First, generate a private and a public key (for each tenant), as described above.
	Run the Sweagle License Generator ($ gradle run).
	Fill all  the required fields. If Lifetime plan is selected, no need to specify Expiration After field.
	Choose the already generated private key( .der file)
	Choose the output folder of the License.txt file.
	Generate. If all ok, a successful message will show.

